/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
      .directive('pageTop', pageTop)
      .controller('pageTopCtrl', pageTopCtrl);

  /** @ngInject */
  function pageTop() {
    return {
      restrict: 'E',
      controller: 'pageTopCtrl',
      templateUrl: 'app/theme/components/pageTop/pageTop.html'
    };
  }

  function pageTopCtrl($scope, $rootScope, $http, $configuration, $translate) {
   

    $scope.changeLanguage = function (lang) {
        $rootScope.lang = lang;
        $configuration.setLanguage(lang);
        $translate.use(lang);
    }

    // $scope.logout = function () {
    //     $http({
    //         method: 'POST',
    //         url: 'login/Logout',
    //         data: "",
    //         headers: { 'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
    //     })
    //     .success(function (result) {
    //         if (result.Status == 1) {
    //             localStorage.removeItem("SpiderWebAdminToken");
    //             window.location.href = $configuration.loginpath;
    //         }
    //         else alert(result.Message);
    //     });
    // }
}
})();