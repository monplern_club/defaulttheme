﻿(function () {
    'use strict';
    angular.module('BlurAdmin.theme.components')
      .controller('showConfirmDialogCtrl', function ($scope, $uibModalInstance, messageDlg, onConfirm) {

          $scope.messageDlg = messageDlg;

          console.log($scope.messageDlg);

          $scope.ok = function () {
              $uibModalInstance.close();
              if (undefined != onConfirm) {
                  onConfirm();
              }
          };

          $scope.cancel = function () {
              $uibModalInstance.dismiss('cancel');
          };

      });

})();
