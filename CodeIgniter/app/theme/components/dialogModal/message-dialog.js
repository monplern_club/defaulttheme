﻿(function () {
    'use strict';
    angular.module('BlurAdmin.theme.components')
      .controller('showMessageDialogCtrl', function ($scope, $uibModalInstance, messageDlg) {

        $scope.messageDlg = messageDlg;
        $scope.ok = function () {
            alert(JSON.stringify(messageDlg));
            $uibModalInstance.close();
        };

        $scope.cancel = function () {
            $uibModalInstance.dismiss('cancel');
        };

    });

})();
