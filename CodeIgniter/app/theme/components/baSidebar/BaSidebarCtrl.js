/**
 * @author v.lugovksy
 * created on 16.12.2015
 */
(function () {
  'use strict';

  angular.module('BlurAdmin.theme.components')
    .controller('BaSidebarCtrl', BaSidebarCtrl);

  /** @ngInject */
  function BaSidebarCtrl($scope, baSidebarService) {

    $scope.menuItems = baSidebarService.getMenuItems();

    /** check access menu **/
    $scope.menuItemsAccess = [];
    //var jsonMenu = JSON.parse($window.sessionStorage.menus); // JSON from Service
   
    var m1 =  {MenuName : 'dashboard'};
    var m2 =  {MenuName : 'maps'};
    var m3 =  {MenuName : 'example'};
    var jsonMenu = [];

    jsonMenu.push(m1);jsonMenu.push(m2);jsonMenu.push(m3);
    angular.forEach($scope.menuItems, function (baSideBarMenu) {
      //  console.log(baSideBarMenu, baSideBarMenu.name);
        angular.forEach(jsonMenu, function (accessMenu) {
          // console.log(accessMenu, "jsonmenu :", jsonMenu)
            if (accessMenu.MenuName === baSideBarMenu.name) {
                $scope.menuItemsAccess.push(baSideBarMenu)
                return;
            }
        })
    })

    // $scope.menuItems = $scope.menuItemsAccess;
    /** check access menu **/


    $scope.defaultSidebarState = $scope.menuItems[0].stateRef;

    $scope.hoverItem = function ($event) {
      $scope.showHoverElem = true;
      $scope.hoverElemHeight =  $event.currentTarget.clientHeight;
      var menuTopValue = 66;
      $scope.hoverElemTop = $event.currentTarget.getBoundingClientRect().top - menuTopValue;
    };

    $scope.$on('$stateChangeSuccess', function () {
      if (baSidebarService.canSidebarBeHidden()) {
        baSidebarService.setMenuCollapsed(true);
      }
    });
  }
})();