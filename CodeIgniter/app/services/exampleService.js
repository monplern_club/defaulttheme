(function () {
    'use strict';
    angular.module('DevDeeThailand.services').factory('exampleService', exampleService);
    function exampleService(baseService) {

        var servicebase = baseService.apiUrl + "Example/";

        return {
            getAllData: function (onComplete) {
                baseService.post(servicebase + 'GetAllData', {}, function (result) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },
            createData: function (model, onComplete) {
                baseService.post(servicebase + 'CreateData', model, function (result) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            },
            deleteListData: function (model, onComplete) {
                baseService.post(servicebase + 'DeleteListData', model, function (result) {
                    if (undefined != onComplete) onComplete.call(this, result);
                });
            }
        };
    }
})();