(function () {
    'use strict';
    angular.module('DevDeeThailand.services').service('baseService', baseService);
    function baseService($http,$rootScope, $configuration, $cookieStore, $uibModal, servicesModule) {
        var self = this;

        /***********page *********/
        var PageIndex = 1;
        var PageSize = 20;
        var SortColumn = '';
        var SortOrder = 'asc';
        var modelSearch;
        var totalPage = 0;
        var totalRecords = 0;
        var overlayCount = 0;

        var MyBase = this;

        this.setSearchModel = function (mSearch) {
            modelSearch = mSearch;
        }
    
        this.setSortColumn = function (sortColumn) {
            SortColumn = sortColumn;
            return SortColumn;
        }
    
        this.setSortOrder = function (sortOrder) {
            SortOrder = sortOrder;
            return SortOrder;
        }
    
        this.getSortColumn = function () {
            return SortColumn;
        }
    
        this.getSortOrder = function () {
            return SortOrder;
        }
    
        this.getFirstPage = function () {
            if (parseInt(PageIndex, 10) !==  1) {
                PageIndex = 1;
            }
            return PageIndex;
        }
    
        this.getNextPage = function () {
            console.log("PageIndex", PageIndex, parseInt(totalPage, 10), parseInt(PageIndex, 10));
            if (parseInt(PageIndex, 10) < parseInt(totalPage, 10)) {
                PageIndex += 1; 
            }
            return PageIndex;
        }
    
        this.getBackPage = function () {
            if (parseInt(PageIndex, 10) > 1) {
                PageIndex -= 1;
            }
            return PageIndex;
        }
    
        this.getLastPage = function () {
            if (parseInt(PageIndex, 10) <= parseInt(totalPage, 10)) {
                PageIndex = totalPage;
            }
            return PageIndex;
        }
    
        this.searchByPage = function () {
            try {
                if ($.isNumeric(PageIndex) === false) {
                    PageIndex = 1;
                } else if (PageIndex < 1) {
                    PageIndex = 1;
                    getFirstPage();
                } else if (PageIndex > totalPage) {
                    PageIndex = totalPage;
                } else {
                    //loadExportPrefix();
                }
            } catch (e) {
                console.log(e);
                return false;
            }
            return PageIndex;
        }
    
        this.setPageSize = function (size) {
            PageSize = size;
            return PageSize;
        }
    
        this.setPageIndex = function (page) {
            PageIndex = page;
            return PageIndex;
        }
    
        this.setSortIcon = function () {
    
            $(".icon").remove();
            $(".sorting").append('<i class="icon fa fa-sort"></i>');
            $(".sorting_asc").append('<i class="icon fa fa-sort-asc"></i>');
            $(".sorting_desc").append('<i class="icon fa fa-sort-desc"></i>');
    
        }
    
        this.showError = function (msg) {
            $('#overlay').hide(); //$('#myWaitModal').modal('hide');
            $('#error_message').html(msg);
            $('#myErrorModal').modal('show');
        }
    
        this.showMessage = function (msg) {
            $('#overlay').hide(); //$('#myWaitModal').modal('hide');
            $('#show_message').html(msg);
            $('#myMessageModal').modal('show');
        }
    
        this.showReloadMessage = function (msg) {
            $('#overlay').hide(); //$('#myWaitModal').modal('hide');
            $('#show_reload_message').html(msg);
            $('#myRelaodMessageModal').modal('show');
        }
    
        this.sort = function (event) {
            var nextSortColumn = $(event.target).attr('sort'); 
            $(".sorting_asc").removeClass("sorting_asc").addClass("sorting");
            $(".sorting_desc").removeClass("sorting_desc").addClass("sorting");
    
            if (SortColumn === nextSortColumn) {
    
                if (SortOrder === "desc") {
                    SortOrder = "asc";
                    $(event.target).removeClass("sorting").addClass("sorting_asc");
                } else {
                    SortOrder = "desc";
                    $(event.target).removeClass("sorting").addClass("sorting_desc");
                }
    
            } else {
                SortColumn = nextSortColumn;
                SortOrder = "desc";
                $(event.target).removeClass("sorting").addClass("sorting_desc");
            }
    
            this.setSortIcon();
        }
    
        this.searchObject = function (url, mSearch, callback) {
            var jsonData = {
                "PageIndex": PageIndex,
                "PageSize": PageSize,
                "SortColumn": SortColumn,
                "SortOrder": SortOrder,
                "mSearch": mSearch
            }; 
    
            MyBase.showOverlay(); //$('#myWaitModal').modal('show');
            MyBase.setSortIcon();
            $http({
                url: url,
                method: "POST", 
                data: jsonData, 
                headers: { 'Content-Type': 'application/json;charset=UTF-8' }
            }).then(function (response) {
                MyBase.hideOverlay();
                totalPage = response.data.toTalPage; //init;
                totalRecords = response.data.totalRecords;
                 
                //callback(response.data.message);
                callback(response);
                //if ('1' == response.data.result) {
                //    totalPage = response.data.toTalPage; //init;
                //    totalRecords = response.data.totalRecords;
                //    callback(response);
                //} else {
                //    this.showError(response.data.message);
                //} 
            }, function (response) {
                MyBase.hideOverlay(); //$('#myWaitModal').modal('hide');
                //this.showError(response.data.message);
            });
        }
    
        this.searchObjectWithoutLoading = function (url, mSearch, callback) {
            var jsonData = {
                "PageIndex": PageIndex,
                "PageSize": PageSize,
                "SortColumn": SortColumn,
                "SortOrder": SortOrder,
                "mSearch": mSearch
            };
    
            //MyBase.showOverlay(); //$('#myWaitModal').modal('show');
    
            $http({
                url: url,
                method: "POST",
                data: jsonData,
                headers: { 'Content-Type': 'application/json;charset=UTF-8' }
            }).then(function (response) {
                //MyBase.hideOverlay();
                totalPage = response.data.toTalPage; //init;
                totalRecords = response.data.totalRecords;
                
                
                //callback(response.data.message);
                callback(response);
                //if ('1' == response.data.result) {
                //    totalPage = response.data.toTalPage; //init;
                //    totalRecords = response.data.totalRecords;
                //    callback(response);
                //} else {
                //    this.showError(response.data.message);
                //} 
            }, function (response) {
                //MyBase.hideOverlay(); //$('#myWaitModal').modal('hide');
                //this.showError(response.data.message);
            });
        }
    
        this.searchAllObject = function (url, mSearch, callback) {
            var jsonData = {
                "PageIndex": 1,
                "PageSize": -1,
                "SortColumn": "",
                "SortOrder": "",
                "mSearch": mSearch
            };
    
            MyBase.showOverlay(); 
    
            $http({
                url: url,
                method: "POST",
                data: jsonData,
                headers: { 'Content-Type': 'application/json;charset=UTF-8' }
            }).then(function (response) {
                MyBase.hideOverlay();
                totalPage = response.data.toTalPage; //init;
                totalRecords = response.data.totalRecords; 
                callback(response);  
            }, function (response) {
                MyBase.hideOverlay();
            });
        }
    
        this.getListPageSize = function (){ 
            var lstData = [{ "Selected": true, "Text": "20 items", "Value": "20" }, { "Selected": false, "Text": "30  items", "Value": "30" }, { "Selected": false, "Text": "50 items", "Value": "50" }, { "Selected": false, "Text": "100 items", "Value": "100" }];
            return lstData;
        }
        
        this.getListPage = function (max){ 
            var lstData = [];
            
            for(var i = 0; i < max; i++){
                var tmpData = {"Text": (i+1), "Value": (i+1), "PageIndex" : (i+1)};
                
                lstData.push(tmpData);
            }
            
            return lstData;
        }
    
        this.SelectAllCheck = function (listData, selected) {
            listData.forEach(function (entry) {
                entry.ITEM_SELECTED = selected;
            });
            return true;
        }
    
        this.SelectAllCheckEx = function (listData, selected) {
            listData.forEach(function (entry) {
                if (entry.ITEM_DISABLE == false) {
                    entry.ITEM_SELECTED = selected;
                }
            });
            return true;
        }
     
        //Search Model  
        this.getCustomerSearch = function () { 
            return { "customerId": "" };
        }
        /***********page *********/



        self.onLoading = onLoading;
        self.sessionExpire = false;
        this.apiUrl = $configuration.rootpath ;//+ "api/";
        this.baseUrl = $configuration.rootpath;
        $rootScope.cntLoading = 0;

        function onLoading(state) {
            if (state) {
                if (++$rootScope.cntLoading == 1) {
                    $("#overlay").show()
                }
            }
            else {
                if (--$rootScope.cntLoading == 0) {
                    $("#overlay").hide()
                }
            }
        }


        this.post = function (url, param, onComplete, nocache) {
            if (self.sessionExpire) return;
            onLoading(true);
            var authToken = localStorage.getItem("SpiderWebAdminToken");
            //console.log("SpiderToken = " + authToken);
            if (undefined == nocache) nocache = false;
            $http({
                method: 'POST',
                url: url + (nocache ? ((occurrences(url, "?", false) > 0 ? '&' : '?') + 'time=' + new Date().getTime()) : ''),
                data: param,
                headers: {
                    'Content-type': 'application/json',
                    'SpiderWebAdminToken': authToken,
                    'Accept-Language': $rootScope.lang //'en'
                }
            }).then(
            function (response) {
                onLoading(false);
                var data = response.data;
                var status = response.status;
                var headers = response.headers;
                var config = response.config;
                var statusText = response.statusText;
                if (undefined != onComplete) onComplete.call(this, data);
            },
            function (response) {
                onLoading(false);
                var data = response.data;
                var status = response.status;
                var headers = response.headers;
                var config = response.config;
                var statusText = response.statusText;
                if (status == 401) {
                    self.handle401(data);
                }
                //
                if (window.console) {
                    console.log(status);
                }
            });
        }

        this.postNoSpinner = function (url, param, onComplete, nocache) {
            if (self.sessionExpire) return;
            var authToken = localStorage.getItem("SpiderWebAdminToken");
            //console.log("SpiderToken = " + authToken);
            if (undefined == nocache) nocache = false;
            $http({
                method: 'POST',
                url: url + (nocache ? ((occurrences(url, "?", false) > 0 ? '&' : '?') + 'time=' + new Date().getTime()) : ''),
                data: param,
                headers: {
                    'Content-type': 'application/json',
                    'SpiderWebAdminToken': authToken,
                    'Accept-Language': $rootScope.lang //'en'
                }
            }).then(
            function (response) {
                var data = response.data;
                var status = response.status;
                var headers = response.headers;
                var config = response.config;
                var statusText = response.statusText;
                if (undefined != onComplete) onComplete.call(this, data);

            },
            function (response) {
                var data = response.data;
                var status = response.status;
                var headers = response.headers;
                var config = response.config;
                var statusText = response.statusText;
                if (status == 401) {
                    self.handle401(data);
                }
                //
                if (window.console) {
                    console.log(status);
                }
            });
        }

        this.handle401 = function (data) {
            if (self.sessionExpire) return;
            self.sessionExpire = true;
            var modal = self.messageModal('danger',data.Message);
            modal.result.finally(function () {
                $http({
                    method: 'POST',
                    url: 'login/Logout',
                    data: "",
                    headers: { 'Content-Type': 'application/x-www-form-urlencoded'}  // set the headers so angular passing info as form data (not request payload)
                })
                .success(function (result) {
                    if (result.Status != 1) {
                        alert(result.Message);
                    }
                    localStorage.removeItem("SpiderWebAdminToken");
                    window.location.href = $configuration.loginpath;
                });
                
            });
        }

        this.showMessageDialog = function (msgText, sizeData) {

            try {
                if (undefined == sizeData) {
                    sizeData = 'lg'; //'sm';

                    try{
                        if(msgText.length < 100){
                            sizeData =  'sm';
                        } 
                    }catch(e){

                    } 
                }

                $uibModal.open({
                    templateUrl: 'app/theme/components/dialogModal/message-dialog.html',
                    controller: 'showMessageDialogCtrl',
                    size: sizeData,
                    windowClass: 'confirm-window',
                    resolve: {
                        messageDlg: function () {
                            return msgText
                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
        }

        this.showErrorDialog = function (msgText, sizeData) {

            try {
                if (undefined == sizeData) {
                    sizeData = 'lg'; //'sm';

                    try{
                        if(msgText.length < 100){
                            sizeData =  'sm';
                        } 
                    }catch(e){

                    } 
                }

                $uibModal.open({
                    templateUrl: 'app/theme/components/dialogModal/error-dialog.html',
                    controller: 'showErrorDialogCtrl',
                    size: sizeData,
                    windowClass: 'confirm-window',
                    resolve: {
                        messageDlg: function () {
                            return msgText
                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
        }

        this.showConfirmDialog = function (msgText, onOK, sizeData) {

            try {
                if (undefined == sizeData) {
                    sizeData = 'sm';
                }

                $uibModal.open({
                    templateUrl: 'app/theme/components/dialogModal/confirm-dialog.html',
                    controller: 'showConfirmDialogCtrl',
                    size: sizeData,
                    windowClass: 'confirm-window',
                    resolve: {
                        messageDlg: function () {
                            return msgText
                        },
                        onConfirm: function () {
                            return onOK
                        }
                    }
                });
            } catch (e) {
                console.log(e);
            }
        }

        this.messageModal = function (type, message, size) {  // type : success, info, warning, danger
            var page;

            if (undefined == size) {
                size = 'sm';
            }

            switch (type) {
                case 1:
                case 'success':
                default:
                    page = 'app/shared/modalTemplates/successModal.html';
                    break;
                case 'info':
                    page = 'app/shared/modalTemplates/infoModal.html';
                    break;
                case 'warning':
                    page = 'app/shared/modalTemplates/warningModal.html';
                    break;
                case 2:
                case 3:
                case 'danger':
                    page = 'app/shared/modalTemplates/dangerModal.html';
                    break;
            }

            return $uibModal.open({
                animation: true,
                templateUrl: page,
                size: size,
                controller: function ($scope, message,$sce) { $scope.message = $sce.trustAsHtml(message); },
                resolve: {
                    message: function () {
                        return message;
                    }
                }
            });

        }
    }
})();