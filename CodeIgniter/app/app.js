'use strict';

angular.module('DevDeeThailand', [
  'ngAnimate',
  'ui.bootstrap',
  'ui.sortable',
  'ui.router',
  'ngTouch',
  'toastr',
  'smart-table',
  "xeditable",
  'ui.slimscroll',
  'ngJsTree',
  'angular-progress-button-styles',  
  'pascalprecht.translate',  
  'ui.select',
  'ui.select2',
  'ngCookies',
  'DevDeeThailand.services',
  'BlurAdmin.theme',
  'BlurAdmin.pages'
]).constant('$configuration', {
  rootpath: $("base").attr("href"),
  loginpath: $("base").attr("href") + "login",
  getLanguage :  function () { var lang = localStorage.getItem("AdminLanguage"); return (null==lang) ? 'en' : lang; },
  setLanguage :  function (lang) { localStorage.setItem("AdminLanguage", lang); },
  getItemPerPage: function () { var count = parseInt(localStorage.getItem("DevDeeThailandItemPerPage")); return isNaN(count) ? 5 : count; },
  setItemPerPage: function (count) { localStorage.setItem("DevDeeThailandItemPerPage", count); }, 
}).config(function ($translateProvider, $configuration) {
  
  $translateProvider.useStaticFilesLoader({
        prefix: $configuration.rootpath + 'app/locales/locale-',
        suffix: '.json'
    }).useSanitizeValueStrategy('sanitizeParameters')// remove the warning from console log by putting the sanitize strategy
    .preferredLanguage($configuration.getLanguage());
});