(function () {
    'use strict';
var app = angular.module('BlurAdmin.pages', ['pascalprecht.translate']);

app.config(function ($translateProvider) {
  $translateProvider.translations('en', {
    TITLE: 'Hello',
    BUTTON_LANG_EN: 'english',
    BUTTON_LANG_TH: 'thai'
  });
  $translateProvider.translations('th', {
    TITLE: 'สวัสดี',
    BUTTON_LANG_EN: 'อังกฤษ',
    BUTTON_LANG_TH: 'ไทย'
  });
  $translateProvider.preferredLanguage('en');
});

});