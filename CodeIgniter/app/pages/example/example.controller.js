﻿(function () {
    'use strict';
    angular.module('BlurAdmin.pages.example')
      .controller('exampleDlgCtrl', ['$scope', '$uibModalInstance' ,
       function ($scope, $uibModalInstance ) {

           $scope.file;
           $scope.progressPercentage;
           $scope.msgErr = "This is Error 55";
           $scope.ok = function () {
				$scope.msgErr = "You press ok";
				$scope.objExample.Email = $scope.objExample.Fname + " " + $scope.objExample.Lname;
			    $uibModalInstance.close($scope.msgErr);
           }; 

           $scope.cancel = function () {
			   $scope.msgErr = "cancel";
               //$uibModalInstance.dismiss('cancel'); // ปิด ธรรมดา
			    $uibModalInstance.close($scope.msgErr); // ส่ง ผล
           };

       } ]);

})();