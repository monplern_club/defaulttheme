(function () {
    'use strict';
  
    var pageName = "example";
    var pageCtrl = pageName + "PageCtrl";

    angular.module('BlurAdmin.pages.example', [])
        .config(routeConfig);
  
    /** @ngInject */
    function routeConfig($stateProvider) {
        $stateProvider
            .state(pageName, {
              url: '/' + pageName,
              templateUrl: 'app/pages/example/example.html',
              controller: pageCtrl,
              title: 'My New Page',
              sidebarMeta: {
                order: 800,
              },
            });
      }
  
      angular.module('BlurAdmin.pages.' + pageName  ).controller(pageCtrl,
      function ($scope,baseService, exampleService,$rootScope,$translate,$uibModal, $configuration) {
  
          var str =  "ABCDEFGHIJKLMNOPQRSTUVWXYZ";
          $scope.formName = "userForm";
          $scope.state = 0; // 0 :List, 1: Edit, 2: Add, 3: Import
          $scope.dataList = [];
          $scope.userListFilter = '';// {'ComputerName': null};
          $scope.alpha = str.split("");
          $scope.userModel = {};
		  $scope.objExample = {};


          $scope.open1 = function() {
            $scope.popup1.opened = true;
          };

          $scope.popup1 = {
            opened: false
          };

          $scope.dt = new Date();
          $scope.date = new Date();
          $scope.options = {
                showWeeks: false
          };
 
          $scope.open = function open() {
                $scope.opened = true;
                console.log(  $scope.opened );
            };
          $scope.opened = false;
          $scope.formats = ['dd-MMMM-yyyy', 'yyyy/MM/dd', 'dd.MM.yyyy', 'shortDate'];
          $scope.format = $scope.formats[0];
          $scope.options = {
                showWeeks: false
            };
          
          $scope.searchParam =       
            {
            "mSearch": {
                "ClientName": "",
                "EndDate": new Date(),
                "StartDate": new Date(),
                "UserName": ""
            },
            "PageIndex": 1,
            "PageSize": $scope.pageSize,
            "SortColumn": "",
            "SortOrder": ""
            };
          $scope.pageSize = $configuration.getItemPerPage();

        
          // For import form
          $scope.displayedCollectionUserList = [];




            //page system 
            $scope.listPageSize = baseService.getListPageSize();
            $scope.TempPageSize = {};
            $scope.TempPageIndex = {};
            $scope.PageSize = baseService.setPageSize(20);;
            $scope.totalPage = 1; //init;
            $scope.totalRecords = 0;
            $scope.PageIndex = 1;
            $scope.SortColumn = baseService.setSortColumn('room_num');
            $scope.SortOrder = baseService.setSortOrder('asc');
            
            $scope.modelSearch = {
                                    "CheckInDate": new Date(),
                                    "CheckOutDate":  new Date(),
                                    "rest_type" : '2'
                                }; 

            $scope.isView = false;

            $scope.sort = function (e) {
                baseService.sort(e); $scope.SortColumn = baseService.getSortColumn();
                $scope.SortOrder = baseService.getSortOrder();
                $scope.reload();
            }

            $scope.getFirstPage = function () { $scope.PageIndex = baseService.getFirstPage(); $scope.reload(); }
            $scope.getBackPage = function () { $scope.PageIndex = baseService.getBackPage(); $scope.reload(); }
            $scope.getNextPage = function () { $scope.PageIndex = baseService.getNextPage(); $scope.reload(); }
            $scope.getLastPage = function () { $scope.PageIndex = baseService.getLastPage(); $scope.reload(); }
            $scope.searchByPage = function () { $scope.PageIndex = baseService.setPageIndex($scope.TempPageIndex.selected.PageIndex); $scope.reload(); }
            $scope.setPageSize = function (data) { $scope.PageSize = baseService.setPageSize($scope.TempPageSize.selected.Value); }
            $scope.loadByPageSize = function () { $scope.PageIndex = baseService.setPageIndex(1); $scope.setPageSize(); $scope.reload(); }
            //page system
  
          ////////////////////////////////////////////////////////////////////////////////////////////
          // Event
          $scope.ShowError = function () {
            baseService.showErrorDialog("Error");
          };

            $scope.ShowMsg = function () {
                baseService.showMessageDialog("Hello world");
            };
        
            $scope.ShowConfirm = function(){

                baseService.showConfirmDialog("Please confrim !", function(){ $scope.ShowMsg(); });
            }

          $scope.onInit = function () { 

             
            /*$scope.listPageSize.forEach(function (entry, index) {
                if (0 === index)
                    $scope.TempPageSize.selected = entry;
            });*/

            $scope.TempPageSize.selected = $scope.listPageSize[2];
            $scope.refreshAll();
          }
  
          $scope.assignSearch = function (text) {
              $scope.userListFilter = text;
          }

          $scope.loadData = function (tableState) { 
                 /****** smartTable  ******/
              if(null != tableState) $scope.tableState = tableState;
              else {
                  tableState = $scope.tableState;
                  tableState.pagination.start = 0;
              }
  
              var pagination = tableState.pagination;
              var start = pagination.start || 0;     // This is NOT the page number, but the index of item in the list that you want to use to display the table.
              var number = pagination.number || 5;  // Number of entries showed per page.
  
              // Page
              $scope.searchParam.PageIndex = (start/number)+1;
              $scope.searchParam.PageSize = number;
              $configuration.setItemPerPage(number);
  
  
              // Search
              if(undefined != tableState.search.predicateObject) {
                  $scope.searchParam.mSearch.GroupName = tableState.search.predicateObject.GroupName;
              }
              else $scope.searchParam.mSearch.GroupName = "";
  
              // Sort
              if(undefined != tableState.sort.predicate) {
                  $scope.searchParam.SortColumn = tableState.sort.predicate;
                  $scope.searchParam.SortOrder = tableState.sort.reverse?"DSC":"ASC";
              }
              else {
                  $scope.searchParam.SortColumn = "";
                  $scope.searchParam.SortOrder = "";
              }
              /****** smartTable  ******/

              $scope.dataList  = "";



              exampleService.getAllData(function(result) { 
                console.log(result);
                  if(result.status === true) {
                      $scope.dataList = result.message;
                      tableState.pagination.numberOfPages = 2;//result.TotalPage; //set the number of pages so the pagination can update
                      tableState.pagination.totalItemCount = 10;// result.TotalRecords;
                  }
                  else {
                      baseService.messageModal(result.Status,result.Message);
                  }
              });
          }
  
          $scope.refreshAll = function () {
              console.log("initial");
              $scope.dataList  = "";
              exampleService.getAllData(function(result) { 
                console.log(result);
                    if(result.status === true) {
                      $scope.dataList = result.message; 

                      $scope.totalPage = result.toTalPage;
                       $scope.listPageIndex = baseService.getListPage(result.toTalPage);
                       console.log( $scope.listPageIndex );
                        $scope.listPageIndex.forEach(function (entry, index) {
                            if ($scope.PageIndex === entry.Value)
                                $scope.TempPageIndex.selected = entry;
                        }); 
                        
                        $scope.totalRecords =  result.totalRecords;
                  }
                  else {
                      baseService.messageModal(result.Status,result.Message);
                  }
              });
          }
  
          $scope.selectAll = function () { 
              angular.forEach($scope.userList, function(value, key) { 
                  var str1 = value.UserName.toUpperCase();
                  var letter = $scope.userListFilter.toUpperCase();
                  if (str1.indexOf(letter.toUpperCase()) != -1) {
                      value.$isChecked = true;
                  }
              }); 
          }
  
          $scope.unSelect = function (text) {
              angular.forEach($scope.userList, function(value, key) { 
                value.$isChecked = false;
              }); 
          }
           
          $scope.delSelect = function (text) {
             
             var ids = new Array();
  
             angular.forEach($scope.userList, function(value, key) { 
                 if(value.$isChecked === true){   
                     ids.push(value.UserID);
                 }
              }); 
  
              if (ids.length > 0) {
                  userService.deleteListUserData( ids, function(result) {  
                      if(result.Status === 1) {
                          $scope.userList = result.Datas;
                      }else {
                          baseService.messageModal(result.Status,result.Message);
                      }
                  }); 
  
              } else {
                  $translate('PleaseSelectData').then(function (message) { 
                       baseService.messageModal("warning",message);
                    }, function (translationId) { 
                      baseService.messageModal("warning",translationId);
                  }); 
              }
          }
		  
		  // วิธี ที่ 1ใช้ controller เดิม แล้ว ส่ง scope เข้าไปเลย จะได้ scope เดียวกัน 
		  $scope.openCustomDlg = function(){
			  $uibModal.open({
				animation: true,
				templateUrl: "app/pages/example/widgets/smartTable.html",
				size: "lg",
				scope: $scope,
			  });
		  }
		  
		   // วิธี ที่  2 ใช้ controller 
		  $scope.openCustomDlg2 = function(){
			  var modalInstance = $uibModal.open({
                    templateUrl: 'app/pages/example/widgets/dialogExample.html',
                    controller: 'exampleDlgCtrl',
                    size: "lg",
                    windowClass: 'confirm-window',
					scope: $scope,
                });
				
				
            modalInstance.result.then(function(result) {
               console.log(result);
            })
			
				
		  }
		   
          $scope.onInit();
  
      });



  })();