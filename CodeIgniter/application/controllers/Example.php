<?php
defined('BASEPATH') OR exit('No direct script access allowed');

 
class Example extends MY_Controller {
 
	public function __construct() {
        parent::__construct();        
		// if(! $this->session->userdata('validated')){
        //     redirect('login');
        // }
    }
	
	public function index()
	{
		
	}
	
	public function GetAllData() {
		// $this->output->set_content_type('application/json');
		$nResult = 0;
		
	  	try{
	  		$this->load->model('ExampleModel','',TRUE);
			$dataPost = json_decode( $this->input->raw_input_stream , true);
			$id =  isset($dataPost['id'])?$dataPost['id']:0;// $this->input->post('ap_id');
			
			$bResult = $this->ExampleModel->GetAllData($dataPost);
			 
			if($bResult){
				$result['status'] = true;
                $result['message'] = $bResult;//$this->lang->line("savesuccess");
                $result['baseController'] = $this->HelloWorld();
                $result['totalRecords'] =  10;
			    $result['toTalPage'] = 2;
			}else{
				$result['status'] = false;
				$result['message'] = $this->lang->line("error_faliure");
			}
			
    	}catch(Exception $ex){
    		$result['status'] = false;
			$result['message'] = "exception: ".$ex;
    	}
	    
		echo json_encode($result, JSON_UNESCAPED_UNICODE);
    }
}

