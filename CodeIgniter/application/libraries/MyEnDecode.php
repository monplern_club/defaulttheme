<?php if (!defined('BASEPATH')) exit('No direct script access allowed');




class MyEnDecode
{
    public function __construct()
    {
        //parent::__construct();
    }

    public function mysql_aes_key($string, $key)
    {
        // echo 1;

        try {
            $iv = mcrypt_create_iv(
                mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC),
                MCRYPT_DEV_URANDOM
            );

            $encrypted = base64_encode(
                $iv .
                    mcrypt_encrypt(
                        MCRYPT_RIJNDAEL_128,
                        hash('sha256', $key, true),
                        $string,
                        MCRYPT_MODE_CBC,
                        $iv
                    )
            );
            return $encrypted;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return $key;
        }
    }

    public function aes_decrypt($encrypted, $key)
    {
        try {
            $data = base64_decode($encrypted);
            $iv = substr($data, 0, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC));

            $decrypted = rtrim(
                mcrypt_decrypt(
                    MCRYPT_RIJNDAEL_128,
                    hash('sha256', $key, true),
                    substr($data, mcrypt_get_iv_size(MCRYPT_RIJNDAEL_128, MCRYPT_MODE_CBC)),
                    MCRYPT_MODE_CBC,
                    $iv
                ),
                "\0"
            );
            return $decrypted;
        } catch (Exception $e) {
            echo 'Caught exception: ',  $e->getMessage(), "\n";
            return $key;
        }
    }
    // $key = 'DevDeeThailand';
    // $string = '1234';
    // echo "Key value : $key / String value : $string <br/>";
    // ​

    // $encyrpt = mysql_aes_key($string, $key);
    // echo "Encrypt Key value : ". $encyrpt. " <br/>";
    // $decyrpt = aes_decrypt($encyrpt, $key);
    // echo "Decrypt Key value : ". $decyrpt. " <br/>";

}
?>
