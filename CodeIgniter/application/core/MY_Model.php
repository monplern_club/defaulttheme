<?php
  
class MY_Model extends CI_Model {
	
    private $tbl_name = 'roomindorm';
	private $id = 'id';
	private static $enum = array(0 => "Empty", 1 => "Daily", 2 => "Monthly");
	private static $room_status = array(0 => "Empty", 1 => "Full");
 
    public function __construct() {
        parent::__construct();
    }
	 
    public function BaseGetAllData($dataPost) {
        $result = array();

        for ($x = 1; $x <= 10; $x++) {
            $array = array(
                "id" => "id" . $x,
                "firstName" => "firstName". $x,
                "lastName" => "lastName". $x,
                "username" => "username". $x,
                "email" => "email". $x,
                "age" => "age". $x,
            );

            $result[] = $array;
        }
        

        return $result;
    }
	

}
?>